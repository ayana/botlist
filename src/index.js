'use strict';

const { Logger, ConfigLoader, StatsDHelper } = require('./util');

const Providers = require('./providers');

class Botlist {
	constructor() {
		this.logger = new Logger();

		this.cfg = new ConfigLoader(['DEBUG'], ['STATSD_DSN']);

		this.statsd = new StatsDHelper();

		this.timer = null;

		this.providers = [];
	}

	async init() {
		try {
			this.cfg.load();
		} catch (e) {
			this.logger.error('BotStatsBase', `ConfigHelper Error: ${e}`);
			process.exit(1);
		}

		// set to debug if DEBUG = true
		if (this.cfg.DEBUG && this.cfg.DEBUG == 'true') this.logger.mode = 'debug';

		for (const Provider of Providers) {
			const provider = new Provider(this.statsd);
			this.logger.info(`BotStatsBase`, `Loaded provider ${provider.getName()}`);
			this.providers.push(provider);
		}

		if (this.cfg.STATSD_DSN) {
			try {
				this.statsd.init(this.cfg.STATSD_DSN);
				this.logger.info('BotStatsBase', `StatsD posting stats to: ${this.statsd.host}:${this.statsd.port} with prefix ${this.statsd.prefix}`);

				this.statsd.on('error', e => {
					this.logger.error('BotStatsBase', `StatsD: ${e}`);
				});
			} catch (e) {
				this.logger.error('BotStatsBase', `StatsD: ${e}`);
			}
		} else this.logger.warn('BotStatsBase', `StatsD Disabled, STATSD_DSN not set`);

		// run at startup
		await this.collect();

		this.timer = setInterval(this.collect.bind(this), 30000);
	}

	async collect() {
		for (const provider of this.providers) {
			try {
				await provider.postStats();
			} catch (e) {
				this.logger.error('BotStatsBase', `Error Posting stats of provider ${provider.getName()}: ${e}`);
			}
		}
	}
}

const base = new Botlist();
global.base = base;

base.init()
.catch(e => {
	console.log(e);
	process.exit(1);
});
