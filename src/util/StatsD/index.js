'use strict';

const url = require('url');
const HotShots = require('hot-shots');

const EventEmitter = require('events');

/*
	* Pulled from ayana-core
	*/
class StatsDHelper extends EventEmitter {
	constructor() {
		super();

		this.host = null;
		this.port = null;
		this.prefix = null;

		this.dsn = null;
		this.cli = null;
		this.connected = false;
	}

	init(dsn) {
		if (!dsn) throw new Error('Missing DSN');

		if (this.cli) {
			this.cli.socket.removeAllListeners();
			this.cli = null;
		}

		const parsed = url.parse(dsn);
		if (parsed.protocol != 'statsd:') throw new Error('DSN not valid statsd: DSN.');

		if (!parsed.hostname || !parsed.port) throw new Error('Invalid DSN: Missing host or port');

		this.host = parsed.hostname;
		this.port = parsed.port;

		this.prefix = parsed.pathname || '';

		if (this.prefix[0] === '/') this.prefix = this.prefix.substr(1);

		this.cli = new HotShots({
			host: this.host,
			port: this.port,
			prefix: this.prefix,
		});

		this.cli.socket.on('error', this.onError.bind(this));
	}

	onError(e) {
		this.emit('error', e);
	}

	timing(...args) {
		if (!this.cli) return;
		return this.cli.timing(...args);
	}

	increment(...args) {
		if (!this.cli) return;
		return this.cli.increment(...args);
	}

	decrement(...args) {
		if (!this.cli) return;
		return this.cli.decrement(...args);
	}

	gauge(...args) {
		if (!this.cli) return;
		return this.cli.gauge(...args);
	}

	histogram(...args) {
		if (!this.cli) return;
		return this.cli.histogram(...args);
	}
}

module.exports = StatsDHelper;
