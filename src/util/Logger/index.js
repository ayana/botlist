'use strict';

module.exports = class Logger {
	constructor(mode) {
		this.mode = 'default';
		if (process.env.AYANA_CORE_LOGGER_MODE) this.mode = process.env.AYANA_CORE_LOGGER_MODE.toString().toLowerCase();
		if (mode) this.mode = mode.toString().toLowerCase();
	}

	getDateTime() {
		let date = new Date();

		return `${date.getFullYear()}.` +
									`${date.getMonth() + 1 < 10 ? `0${(date.getMonth() + 1)}` : date.getMonth() + 1}.` +
									`${date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()} ` +
									`${date.getHours() < 10 ? `0${date.getHours()}` : date.getHours()}:` +
									`${date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()}:` +
									`${date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds()}`;
	}

	info(from, msg) {
		this.write(`${this.getDateTime()} [${from}/INFO]: ${msg}`);
	}

	error(from, msg) {
		this.write(`${this.getDateTime()} [${from}/ERROR]: ${msg}`);
	}

	fatel(from, msg) {
		this.write(`${this.getDateTime()} [${from}/FATEL]: ${msg}`);
	}

	warn(...args) {
		this.warning(...args);
	}

	warning(from, msg) {
		this.write(`${this.getDateTime()} [${from}/WARNING]: ${msg}`);
	}

	debug(from, msg) {
		if (this.mode !== 'debug') return;
		this.write(`${this.getDateTime()} [${from}/DEBUG]: ${msg}`);
	}

	write(msg) {
		if (process.env.NODE_ENV && process.env.NODE_ENV === 'development') console.log(`${msg}`);
		process.stdout.write(`${msg}\n`);
	}
};
