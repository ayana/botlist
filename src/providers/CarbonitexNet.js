'use strict';

const superagent = require('superagent');

const ProviderBase = require('./ProviderBase');

const nameFormatRegex = / |-|\./g;

class CarbonitexNet extends ProviderBase {
	async postStats() {
		const resp = await superagent.get('https://www.carbonitex.net/discord/api/listedbots');

		// clean bot names and post stats
		for (const bot of resp.body) this.statsd.gauge(`carbon.${bot.name.toString().replace(nameFormatRegex, '_')}`, parseInt(bot.servercount));
	}

	getName() {
		return 'Carbonitex.net';
	}
}

module.exports = CarbonitexNet;
