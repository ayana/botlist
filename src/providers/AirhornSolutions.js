'use strict';

const https = require('https');

const ProviderBase = require('./ProviderBase');

class AirhornSolutions extends ProviderBase {
	async postStats() {
		let stats = await this.getStats();
		if (!stats.unique_guilds) throw new Error('Guild property in airhorn.solutions stats not found.');
		this.statsd.gauge(`airhornsolutions`, parseInt(stats.unique_guilds));
	}

	getStats() {
		return new Promise((resolve, reject) => {
			let req = https.get('https://airhorn.solutions/events', res => {
				res.on('data', d => {
					req.end();
					res.destroy();

					try {
						let json = JSON.parse(d.toString().split('\n')[2].substr(6));
						resolve(json);
					} catch (e) {
						reject(new Error('Failed to read response from https://airhorn.solutions/events.'));
					}
				});

				res.on('error', e => reject(e));
			});
			req.on('error', e => reject(e));
		});
	}

	getName() {
		return 'Airhorn.solutions';
	}
}

module.exports = AirhornSolutions;
