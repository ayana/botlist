'use strict';

const superagent = require('superagent');

const ProviderBase = require('./ProviderBase');

const nameFormatRegex = / |-|\./g;

class DiscordBotsOrg extends ProviderBase {
	async postStats() {
		let botCount = 0; let step = 0; let less500 = false;
		do {
			const resp = await superagent.get(`https://discordbots.org/api/bots?limit=500&sort=server_count&fields=username,server_count&offset=${step * 500}`);
			botCount = resp.body.results.length || 0;
			for (const bot of resp.body.results) {
				// break out after we react first bot < 500 server count
				if (parseInt(bot.server_count) < 500) {
					less500 = true;
					break;
				}

				this.statsd.gauge(`discordbots_org.${bot.username.toString().replace(nameFormatRegex, '_')}`, parseInt(bot.server_count));
			}
			step++;

			if (less500) break;
		} while (botCount > 499);
	}

	getName() {
		return 'DiscordBots.org';
	}
}

module.exports = DiscordBotsOrg;
