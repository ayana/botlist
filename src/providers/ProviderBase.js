'use strict';

class ProviderBase {
	constructor(statsd) {
		this.statsd = statsd;
	}

	async postStats() {
		throw new Error('postStats() not overridden in ProviderBase.');
	}

	getName() {
		return 'getName() not overridden in ProviderBase';
	}
}

module.exports = ProviderBase;
