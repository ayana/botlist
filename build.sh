#!/bin/bash

#VARS
image=registry.gitlab.com/ayana/botlist
tag=latest

#SCRIPT PATH
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null

mc_build() {
 cd $(dirname "$0")

 echo -n "Enter docker tag ${image}:[latest]: "
 read tag

 echo "Building ${image}:${tag}..."

 docker build -t "${image}:${tag}" .
}

mc_push() {
 echo -n "Enter docker tag ${image}:[latest]: "
 read tag

 echo "Pushing ${image}:${tag}..."

 docker push "${image}:${tag}"
}

mc_buildpush() {
 echo -n "Enter docker tag ${image}:[latest]: "
 read tag

 echo "Building ${image}:${tag}..."

 docker build -t "${image}:${tag}" .

 echo "Pushing ${image}:${tag}..."

 docker push "${image}:${tag}"
}


case $1 in
 build)
  mc_build
  ;;
 push)
  mc_push
  ;;
 buildpush)
  mc_buildpush
  ;;
 *)
  echo "${0} build|push|buildpush"
  ;;
esac
