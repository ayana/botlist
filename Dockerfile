FROM node:8.9-alpine

LABEL maintainer hcgrandon "randon@ayana.io"

#install required packages
RUN apk add --no-cache git make g++ python

#create ayana user
RUN adduser -Sh /botlist botlist

#copy source
COPY . /botlist

#fix perms
RUN chown -R botlist /botlist

#use user and workdir
USER botlist
WORKDIR /botlist

#run npm
RUN yarn install --production && \
    yarn cache clean

CMD ["node", "/botlist/src/index.js"]
